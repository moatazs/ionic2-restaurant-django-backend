from django.shortcuts import render
# Create your views here.
from djoser.views import RegistrationView
from djoser import signals
from mysite import settings
from django.dispatch import receiver
# class CustomRegistrationView(RegistrationView):

#     def perform_create(self, serializer):
#         user = serializer.save()
#         user.is_active = False
#         user.save()
#         signals.user_registered.send(sender=self.__class__, user=user, request=self.request)
#         # if settings.get('SEND_ACTIVATION_EMAIL'):
#         #     self.send_activation_email(user)
#         # elif settings.get('SEND_CONFIRMATION_EMAIL'):
#         #     self.send_confirmation_email(user)


@receiver(signals.user_registered)
def my_callback(sender, **kwargs):
    user = kwargs['user']
    if user:
        mobile = user.phone_number
        device_id = user.device_ident
        user.token = ''
        user.is_verified =  False
        user.save()
       
            #create passcode and send response
        pl = random.sample([1,2,3,4,5,6,7,8,9,0],5)
        passcode = ''.join(str(p) for p in pl)
        passcode_entry, created = PasscodeVerify.objects.update_or_create(phone_number=mobile,  defaults={'device_ident' : device_id, 'passcode' : passcode,'is_verified' : False})
        print "PASSCODE: " + str(passcode)









from customauth.models import MyUser
from models import PasscodeVerify

from django.http import HttpResponse
from django.views.generic import View

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


import random
import binascii
import os
import re
from rest_framework_jwt.settings import api_settings

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

def jwt_response_payload_handler(user=None, data=None):
    if user:
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return {
            'token': token,
            'user': data
        }
    return None

class CustomRegistrationView(RegistrationView):

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        final_response = jwt_response_payload_handler(user,serializer.data)
        return Response(final_response, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        user = serializer.save()
        signals.user_registered.send(sender=self.__class__, user=user, request=self.request)
        # if settings.get('SEND_ACTIVATION_EMAIL'):
        #     self.send_activation_email(user)
        # elif settings.get('SEND_CONFIRMATION_EMAIL'):
        #     self.send_confirmation_email(user)
        return user
        # user = serializer.save()
        # user.is_active = False
        # user.save()
        # signals.user_registered.send(sender=self.__class__, user=user, request=self.request)
        # if settings.get('SEND_ACTIVATION_EMAIL'):
        #     self.send_activation_email(user)
        # elif settings.get('SEND_CONFIRMATION_EMAIL'):
        #     self.send_confirmation_email(user)

@csrf_exempt
@api_view(['POST'])
def is_verified(request):
    if request.method == 'POST':
        
        try:
            username = request.POST['username']
        except:
            response_data = {'code' : 'Invalid Data - expecting username' }
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

        try:
            user = MyUser.objects.get(username = username)
            response_data = {'is_verified' : user.is_verified }
            return Response(response_data,status=status.HTTP_200_OK)
        except MyUser.DoesNotExist:
            response_data['code'] = '1'
            response_data['msg'] ='Invalid Data - expecting phone_number and device_id'
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def create_new_pin(request):
    if request.method == 'POST':
        
        response_data = {'code' : 'Invalid Data - expecting phone_number and device_id' }
        
        #Raise exception in case bad data
        try:
            mobile = request.POST['phone_number']
            device_id = request.POST['device_id']
            
        except:
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
        # validate = re.search('^[789]\d{9}$', mobile)
        # if validate is None:
        #     return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
        #check user already
        try:
            user = MyUser.objects.get(phone_number = mobile, device_ident = device_id)
        except MyUser.DoesNotExist:
            user = ''
        
        if user:
            user.token = ''
            response_data['code'] = 'Re-Registering'
            user.is_verified =  False
            user.save()
        else:
            response_data['code']  = "1"
            response_data['msg'] = 'User does Not exist. Please sign up'
            return Response(response_data, status = status.HTTP_400_BAD_REQUEST)
            
            #create passcode and send response
        pl = random.sample([1,2,3,4,5,6,7,8,9,0],5)
        passcode = ''.join(str(p) for p in pl)

        try:
            #create entry in passcode table for verification
            passcode_entry, created = PasscodeVerify.objects.update_or_create(phone_number=mobile,  defaults={'device_ident' : device_id, 'passcode' : passcode,'is_verified' : False})

        except:
            response_data['is_verified']  = "Expire"
            return Response(response_data, status = status.HTTP_400_BAD_REQUEST)
        response_data['passcode'] = passcode
        response_data['code'] = 'Success'
            
            # SMS api to send passcode
        return Response(response_data, status = status.HTTP_201_CREATED)

import json
@csrf_exempt
@api_view(['POST'])
def verify_and_create(request):
    #verify passcode in PasscodeVerify table

    response_data = {'code' : 'Invalid Data - expecting mobile, device_id and passcode' }
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            mobile = data['phone_number']
            device_id = data['device_id']
            passcode = data['passcode']
            
        except:
            return Response(response_data,status=status.HTTP_400_BAD_REQUEST)

        try:
            valid = PasscodeVerify.objects.get(phone_number = mobile, device_ident = device_id , passcode = passcode, is_verified = False)
        except PasscodeVerify.DoesNotExist:
            response_data['code'] = 'Invalid/Expired passcode'
            return Response(response_data,status=status.HTTP_400_BAD_REQUEST)

        if valid:
            valid.is_verified = True
            valid.save()


        #Generate token 
        token = binascii.hexlify(os.urandom(20)).decode()
        created = ''
        #update or create user device_id and token
        try:
            user,created = MyUser.objects.update_or_create(phone_number = mobile,  defaults = {'device_ident' : device_id , 'token' : token ,'is_verified' : True})
        except:
            response_data['code'] = 'User creation error'
            return Response(response_data,status=status.HTTP_400_BAD_REQUEST)

        response_data['code'] = 'Success User created'
        response_data['token'] = token

        return Response(response_data,status=status.HTTP_201_CREATED)

#Done
#Re-register request authorization


# Next : SMS API integration, 
#validate mobile number format in Post
#Token as one to one field user
#Encrypt device-ident
#Format code for Github and upload