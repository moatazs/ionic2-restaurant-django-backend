from django.contrib import admin

# Register your models here.
from customauth.models import MyUser,PasscodeVerify,RestaurantOwner, RestaurantChef, RestaurantWaiter
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django import forms
class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    class Meta:
        model = MyUser
        fields = [f.name for f in MyUser._meta.fields]
        exclude = ['created_on']

class UserAdmin(BaseUserAdmin):
    form =UserChangeForm
    list_display =  [f.name for f in MyUser._meta.fields]
    fieldsets = BaseUserAdmin.fieldsets + (
            (None, {'fields': ('user_type','is_verified',)}),
    )


admin.site.register(MyUser, UserAdmin)
admin.site.register(PasscodeVerify)
admin.site.register(RestaurantOwner)
admin.site.register(RestaurantChef)
admin.site.register(RestaurantWaiter)