from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from restaurant.models import Restaurant

class MyUser(AbstractUser):
    date_of_birth = models.DateField(blank=True)
    phone_number = PhoneNumberField(unique=True)
    is_verified = models.BooleanField(default=False)
    device_ident = models.CharField(max_length = 50)
    created_on = models.DateTimeField(auto_now_add=True)
    token = models.CharField(max_length = 50,default = 'xyz')

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)

    type_choices = (
        ('CUSTOMER', 'CUSTOMER'),
        ('OWNER', 'OWNER'),
        ('WAITER', 'WAITER'),
        ('CHEF', 'CHEF'),
    )
    user_type = models.CharField(max_length=8,
                                 choices=type_choices,
                                 default='CUSTOMER')

    REQUIRED_FIELDS = ['date_of_birth','phone_number','gender','email']


class PasscodeVerify(models.Model):
    phone_number = PhoneNumberField(unique=True)
    device_ident = models.CharField(max_length = 20)
    passcode = models.CharField(max_length = 5,default='00000')
    is_verified = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return (str(self.mobile) + ',' + self.passcode)

class RestaurantOwner(models.Model):
    user = models.OneToOneField(MyUser, on_delete=models.CASCADE)
    restaurant = models.OneToOneField(Restaurant, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "owner: " + str(self.user.username)

class RestaurantChef(models.Model):
    user = models.OneToOneField(MyUser, on_delete=models.CASCADE)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "chef: " + str(self.user.username)

class RestaurantWaiter(models.Model):
    user = models.OneToOneField(MyUser, on_delete=models.CASCADE)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "waiter: " + str(self.user.username)



# specual user restaurant owner which stores restuarnat id or pk or exact name