def jwt_response_payload_handler(token, user=None, request=None):
    """
    Returns the response data for both the login and refresh views.
    Override to return a custom response such as including the
    serialized representation of the User.
    Example:
    def jwt_response_payload_handler(token, user=None, request=None):
        return {
            'token': token,
            'user': UserSerializer(user, context={'request': request}).data
        }
    """
    response = {
        'token': token,
        'verified' : user.is_verified,
        'user_type' : user.user_type,
        'phone_number' : str(user.phone_number)
    }
    if user.user_type == "OWNER":
        response['details'] = {
            'restaurant': user.restaurantowner.restaurant.pk
        }
    elif user.user_type == 'WAITER':
    
        response['details'] = {
                    'restaurant': user.restaurantwaiter.restaurant.pk
        }
    return response;