from django.conf.urls import url,include
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token,refresh_jwt_token
from customauth import views
from django.conf.urls.static import static
from django.conf import settings
from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(title="Server Monitoring API")

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^nested_admin/', include('nested_admin.urls')),
    # url(r'^auth/login', rest_framework_jwt.views.obtain_jwt_token),  # using JSON web token
    url(r'^schema/$', schema_view),


    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^auth/', include('djoser.urls.authtoken')),
    url(r'^register-jwt/$', views.CustomRegistrationView.as_view()),
    url(r'^requestcode/$',views.create_new_pin),
    url(r'^verifycode/$', views.verify_and_create),
    url(r'^isverified/$', views.is_verified),
    url(r'^orders/', include('orders.urls')),
    url(r'^restaurant/', include('restaurant.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
