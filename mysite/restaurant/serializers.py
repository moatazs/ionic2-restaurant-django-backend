from rest_framework import serializers
from restaurant.models import Restaurant,Table,Seat
from orders.serializers import CategorySerializer
# class RestaurantSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Restaurant
#         fields = '__all__'

class RestaurantNestedSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = Restaurant
        fields = ['id', 'phone_number', 'restaurant_name', 'image', 'qr_string', 'categories']

class TableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Table
        fields = '__all__'


class SeatSerializer(serializers.ModelSerializer):

    class Meta:
        model = Seat
        fields = '__all__'



