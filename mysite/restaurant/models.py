from __future__ import unicode_literals

from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from stdimage.models import StdImageField
import os
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
# def get_image_path(instance, filename):
#     return os.path.join('photos', str(instance.id), filename)

class Restaurant(models.Model):
    phone_number = PhoneNumberField(unique=True)
    restaurant_name = models.CharField(max_length = 100)
    restaurant_email = models.EmailField(blank=True, null=True)
    long_position   = models.DecimalField (max_digits=9, decimal_places=6,blank=True, null=True)
    lat_position   = models.DecimalField (max_digits=9, decimal_places=6,blank=True, null=True)
    image = StdImageField(upload_to='pic_folder/',blank=True, null=True,
                          variations={'thumbnail': {'width': 300, 'height': 150}})

    facebook_page = models.TextField(validators=[URLValidator()],blank=True, null=True)
    twitter_handle = models.CharField(max_length=80, blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    address = models.CharField(max_length=200, blank=True, null=True)
    postal_code = models.CharField(max_length=20, blank=True, null=True)

    gmap_url = models.CharField(max_length=200, blank=True, null=True)

    qr_string = models.CharField(max_length=100, blank=True, null=True)

    # owner = models.OneToOneField(RestaurantOwner,null=True,on_delete=models.SET_NULL)

    # waiters = models.ManyToManyField(RestaurantWaiter,null=True, related_name='restaurants')

    # chefs = models.ManyToManyField(RestaurantChef, null=True, related_name='restaurants')

    def get_location(self):
        output = "long:" + str(self.long_position) + " , " + "lat:" + str(self.lat_position)
        return output

    def __str__(self):
        return self.restaurant_name


class Table(models.Model):
    number = models.IntegerField(default=0)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    qr_string = models.CharField(max_length=100,blank=True, null=True) # might not be needed
    #restaurantPk-table-# , # is randomly generated by person 
    #creating the QR code prints.

    class Meta:
        unique_together = ('number', 'restaurant')
        ordering = ['number']

    def __str__(self):
        return self.number

    def __unicode__(self):
        return '%d' % (self.number)



class Seat(models.Model):
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    number = models.IntegerField(default=0)
    qr_string = models.CharField(max_length=100,unique=True)
    #restaurantPk-seat-# , # is randomly generated by person 
    #creating the QR code prints. no table # cuz seats can be added dynamically
    #to any table and removed. seat # auto generated by person generating qr codes

    class Meta:
        unique_together = ('number', 'table')
        ordering = ['number']

    def __str__(self):
        return "table: " + str(self.table.number) + "-seat: " + self.number

    def __unicode__(self):
        return '%d' % (self.number)




