from django.shortcuts import render

from rest_framework import viewsets,permissions
from restaurant.models import Table,Seat,Restaurant
from restaurant.serializers import TableSerializer,SeatSerializer,RestaurantNestedSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import detail_route, list_route
from customauth.models import RestaurantOwner
from django.core.exceptions import ObjectDoesNotExist

class RestaurantNestedViewSet(viewsets.ModelViewSet):
    """
    returnes limited info about restaurant and related categories.
    This is the entry point to the app
    User scans his seat gets the Restaurant's Id and gets info abt restaurant and categories for the menu
    """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantNestedSerializer
    permission_classes = (
        # permissions.IsAuthenticatedOrReadOnly,
        permissions.AllowAny,
        
        # permissions.IsAuthenticated,
        )



class TableViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides 'list', 'create', 'retrieve',
    'update' and 'destroy' actions.
    """
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    permission_classes = (
        # permissions.IsAuthenticatedOrReadOnly,
        permissions.AllowAny,
        
        # permissions.IsAuthenticated,
        )

    def get_queryset(self):
        """
        Optionally restricts the returned tables to the restaurant owned by the logged in user
        """

        queryset = Table.objects.all().order_by('number')
        user = self.request.user
        if not user.is_anonymous() and user.user_type=="OWNER" :
            queryset = queryset.filter(restaurant=user.restaurantowner.restaurant)
        return queryset

    def create(self, request, *args, **kwargs):
        user = self.request.user
        if not user.is_anonymous() and user.user_type=="OWNER" :
            print request.data
            request.data['restaurant'] = user.restaurantowner.restaurant.pk
            print request.data
        return super(TableViewSet, self).create(request, *args, **kwargs)

    # @list_route(methods=['get'])    
    # def restaurant_tables(self, request):
    #     """
    #     returns all restauran tables by using the logged in user who is the owner 
    #     of the restuanrat. No info passed needed except authentication token
    #     """
    #     user_instance = request.user
    #     print user_instance
    #     if user_instance:
    #         try:
    #             owner = user_instance.restaurantowner
    #         except Restaurant.DoesNotExist:
    #             return Response('User is not a restaurant owner',
    #                         status=status.HTTP_400_BAD_REQUEST)

    #         print owner
    #         if owner:
    #             try:
    #                 owner_restaurant = owner.restaurant
    #             except RestaurantOwner.DoesNotExist:
    #                 return Response('RestaurantOwner has no restaurant',
    #                             status=status.HTTP_400_BAD_REQUEST)

    #             print owner_restaurant
    #             if owner_restaurant:
    #                 tables = Table.objects.filter(restaurant=owner_restaurant)
    #                 serializer = TableSerializer(tables,many=True)
    #                 return Response(serializer.data)
    #     return Response('error',
    #                         status=status.HTTP_400_BAD_REQUEST)

        
    @list_route(methods=['post'])
    def multiple_tables(self, request):
        serializer = TableSerializer(data=request.data,many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                          

class SeatViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides 'list', 'create', 'retrieve',
    'update' and 'destroy' actions.
    """
    queryset = Seat.objects.all()
    serializer_class = SeatSerializer
    permission_classes = (
        # permissions.IsAuthenticatedOrReadOnly,
        permissions.AllowAny,
        )
    def get_queryset(self):
        """
        Optionally restricts the returned seats to a given table pk,
        by filtering against a `table pk` query parameter in the URL.
        """
        queryset = Seat.objects.all().order_by('number')
        tablepk = self.request.query_params.get('tablepk', None)
        if tablepk is not None:
            queryset = queryset.filter(table__pk=tablepk)
        return queryset

    # @detail_route(methods=['get'])
    # def table_seats(self, request, pk=None):
    #     """
    #     returns all seats for the givin pk which is the pk of the table not the table #

    #     """
    #     if pk:
    #         try:
    #             table = Table.objects.get(pk=pk)
    #         except ObjectDoesNotExist:
    #             return Response('Invaild table Id', status=status.HTTP_400_BAD_REQUEST)
    #         seats = table.seat_set
    #         serializer = SeatSerializer(seats,many=True)
    #         return Response(serializer.data)
    #     else:
    #         return Response('No table id found', status=status.HTTP_400_BAD_REQUEST)


    @list_route(methods=['post'])
    def add_multiple_seats(self, request):
        serializer = SeatSerializer(data=request.data,many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'])
    def remove_seat_byqr(self, request):
        """ deleted a single seat based on the qr code value passed"""
        seat_qr_code = request.POST.get('seat_qr_code')
        if seat_qr_code:
            try:
                seat = Seat.objects.get(qr_string=seat_qr_code)
                seat.delete()
                return Response('Seat has been deleted', status=status.HTTP_200_OK)
            except ObjectDoesNotExist:
                return Response('No seat found with corresponding qr code scanned', 
                    status=status.HTTP_404_NOT_FOUND)
        return Response('POST method required [seat_qr_code]', status=status.HTTP_400_BAD_REQUEST)

