from django.conf.urls import url, include
from restaurant import views
from rest_framework.routers import DefaultRouter

# Create a router and register our viewsets with it.


router = DefaultRouter()

router.register(r'nestedrestaurant', views.RestaurantNestedViewSet)
router.register(r'tables', views.TableViewSet)
router.register(r'seats', views.SeatViewSet)


# snippet_list = SnippetViewSet.as_view({
#     'get': 'list',
#     'post': 'create'
# })

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
  # url(r'^snippets/$', snippet_list, name='snippet-list'),
    url(r'^', include(router.urls)),
]