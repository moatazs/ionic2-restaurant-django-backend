from django.contrib import admin

from restaurant.models import Restaurant, Table, Seat
class RestaurantAdmin(admin.ModelAdmin):
    list_display =  [f.name for f in Restaurant._meta.fields]

admin.site.register(Restaurant,RestaurantAdmin)

class SeatInline(admin.TabularInline):
    model = Seat
    extra=0

class TableAdmin(admin.ModelAdmin):
    list_display =  [f.name for f in Table._meta.fields]
    inlines = [
        SeatInline,
    ]
    extra=0

admin.site.register(Table,TableAdmin)

class SeatAdmin(admin.ModelAdmin):
    list_display =  [f.name for f in Seat._meta.fields]

admin.site.register(Seat,SeatAdmin)