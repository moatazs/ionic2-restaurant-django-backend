from django.contrib import admin

from orders.models import Category,Option,Item,ItemOption,Order,SelectedItem



import nested_admin

class ItemOptionInlineNested(nested_admin.NestedTabularInline):
    model = ItemOption
    extra = 0

class OptionInlineNested(nested_admin.NestedTabularInline):
    model = Option
    inlines = [ItemOptionInlineNested]
    extra = 0

class ItemInlineNested(nested_admin.NestedTabularInline):
    model = Item
    inlines = [OptionInlineNested]
    extra = 0

class CategoryAdminNested(nested_admin.NestedModelAdmin):
    inlines = [ItemInlineNested]
    list_display =  [f.name for f in Category._meta.fields]
    extra = 0

admin.site.register(Category, CategoryAdminNested)





class ItemAdminInline(admin.TabularInline):
    model = Item

class CategoryAdmin(admin.ModelAdmin):
    list_display =  [f.name for f in Category._meta.fields]
    inlines = (ItemAdminInline,)

# admin.site.register(Category,CategoryAdmin)
class ItemOptionInline(admin.TabularInline):
    model = ItemOption
    extra=0

class OptionAdmin(admin.ModelAdmin):
    list_display =  [f.name for f in Option._meta.fields]
    inlines = [
        ItemOptionInline,
    ]

admin.site.register(Option,OptionAdmin)

# class ItemAdmin(admin.ModelAdmin):
#     list_display =  [f.name for f in Item._meta.fields]

# admin.site.register(Item,ItemAdmin)

class ItemOptionAdmin(admin.ModelAdmin):
    list_display =  [f.name for f in ItemOption._meta.fields]

admin.site.register(ItemOption,ItemOptionAdmin)


class SelectedItemAdmin(admin.ModelAdmin):
    list_display =  [f.name for f in SelectedItem._meta.fields]

admin.site.register(SelectedItem,SelectedItemAdmin)


class SelectedItemInline(admin.TabularInline):
    model = SelectedItem
    extra=0

class OrderAdmin(admin.ModelAdmin):
    list_display =  [f.name for f in Order._meta.fields]
    inlines = [
        SelectedItemInline,
    ]
    extra=0

admin.site.register(Order,OrderAdmin)



class ItemInlineAdminNested(nested_admin.NestedModelAdmin):
    list_display =  [f.name for f in Item._meta.fields]
    inlines = [OptionInlineNested,]
    extra = 0

admin.site.register(Item, ItemInlineAdminNested)

