from django.shortcuts import render

from rest_framework import viewsets,permissions
from orders.models import Category,Option,Item,ItemOption,Order,SelectedItem
from restaurant.models import Restaurant
from orders.serializers import CategorySerializer,CategoryNestedSerializer,ItemNestedSerializer,OrderNestedSerializer,SelectedItemSerializer,SelectedItemNestedSerializer,DetailedOrderNestedSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import detail_route, list_route
from django.core.exceptions import ObjectDoesNotExist



class CategoryNestedViewSet(viewsets.ModelViewSet):
    """
   return categories with all info of children
    """
    queryset = Category.objects.all()
    serializer_class = CategoryNestedSerializer
    permission_classes = (
        # permissions.IsAuthenticatedOrReadOnly,
        permissions.AllowAny,
        
        # permissions.IsAuthenticated,
        )

class ItemNestedViewSet(viewsets.ModelViewSet):
    """
   return Items with all info of children
    """
    queryset = Item.objects.all()
    serializer_class = ItemNestedSerializer
    permission_classes = (
        # permissions.IsAuthenticatedOrReadOnly,
        permissions.AllowAny,
        
        # permissions.IsAuthenticated,
        )

class SelectedItemsViewSet(viewsets.ModelViewSet):
    """
   return Items with all info of children
    """
    queryset = SelectedItem.objects.all()
    serializer_class = SelectedItemSerializer
    permission_classes = (
        # permissions.IsAuthenticatedOrReadOnly,
        permissions.AllowAny,
        
        # permissions.IsAuthenticated,
        )

    @list_route()
    def details(self, request):
        queryset = SelectedItem.objects.all()

        serializer = SelectedItemNestedSerializer(queryset, many=True)
        return Response(serializer.data)

class OrderNestedViewSet(viewsets.ModelViewSet):
    """
   return orders with all info of children of selecteditems
    """
    queryset = Order.objects.all()
    serializer_class = OrderNestedSerializer
    permission_classes = (
        # permissions.IsAuthenticatedOrReadOnly,
        permissions.AllowAny,
        
        # permissions.IsAuthenticated,
        )

    def get_queryset(self):
        """
        Optionally restricts the returned orders to customer who ordered it
        """

        queryset = Order.objects.all()
        user = self.request.user
        if not user.is_anonymous():
            queryset = queryset.filter(customer=user)
        return queryset

    @detail_route(methods=['get'])
    def details(self, request, pk=None):
        order = Order.objects.get(pk=pk)
        serializer = DetailedOrderNestedSerializer(order)
        return Response(serializer.data)


    # could add method for returning all categories for a certain restaurant pk
