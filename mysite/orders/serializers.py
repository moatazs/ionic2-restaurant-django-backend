from rest_framework import serializers
from orders.models import Category,Option,Item,ItemOption,Order,SelectedItem
from restaurant.models import Restaurant


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'


class SelectedItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = SelectedItem
        fields = '__all__'

class OptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Option
        fields = '__all__'


class ItemOptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = ItemOption
        fields = '__all__'

class ItemOptionWithStringOptionsSerializer(serializers.ModelSerializer):
    option = serializers.SerializerMethodField()

    class Meta:
        model = ItemOption
        fields = ('id', 'value', 'price', 'hint', 'option')

    def get_option(self, obj):
        return obj.option.name


class SelectedItemNestedWithStringOptionInItemOptionSerializer(serializers.ModelSerializer):
    itemoptions = ItemOptionWithStringOptionsSerializer(many=True, read_only=True)

    class Meta:
        model = SelectedItem
        fields = ['id', 'order', 'item', 'comments', 'created', 'itemoptions']

class SelectedItemNestedSerializer(serializers.ModelSerializer):
    itemoptions = ItemOptionSerializer(many=True, read_only=True)

    class Meta:
        model = SelectedItem
        fields = ['id', 'order', 'item', 'comments', 'created', 'itemoptions']

class OrderNestedSerializer(serializers.ModelSerializer):
    selecteditems = SelectedItemSerializer(many=True)

    class Meta:
        model = Order
        fields = ['id', 'customer', 'seat', 'closed', 'created', 'paid', 'selecteditems']

    def create(self, validated_data):
        selecteditems_data = validated_data.pop('selecteditems')
        order = Order.objects.create(**validated_data)
        for selecteditem_data in selecteditems_data:
            SelectedItem.objects.create(order=order, **selecteditem_data)
        return order

class DetailedOrderNestedSerializer(serializers.ModelSerializer):
    selecteditems = SelectedItemNestedWithStringOptionInItemOptionSerializer(many=True)

    class Meta:
        model = Order
        fields = ['id', 'customer', 'seat', 'closed', 'created', 'paid', 'selecteditems']
    # add this later on
    # def create(self, validated_data):
    #     selecteditems_data = validated_data.pop('selecteditems')
    #     order = Order.objects.create(**validated_data)
    #     for selecteditem_data in selecteditems_data:
    #         SelectedItem.objects.create(order=order, **selecteditem_data)
    #     return order

class OptionNestedSerializer(serializers.ModelSerializer):
    itemoptions = ItemOptionSerializer(many=True, read_only=True)

    class Meta:
        model = Option
        fields = ['id', 'name', 'required', 'item', 'itemoptions']

class ItemNestedSerializer(serializers.ModelSerializer):
    options = OptionNestedSerializer(many=True, read_only=True)

    class Meta:
        model = Item
        fields = ['id', 'name', 'description', 'category', 'image', 'price', 'options']

class CategoryNestedSerializer(serializers.ModelSerializer):
    items = ItemNestedSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ['id', 'name', 'restaurant', 'image', 'description', 'items']