from django.conf.urls import url, include
from orders import views
from rest_framework.routers import DefaultRouter
from orders import views
# Create a router and register our viewsets with it.


router = DefaultRouter()
router.register(r'nestedcategories', views.CategoryNestedViewSet)
router.register(r'nesteditems', views.ItemNestedViewSet)
router.register(r'nestedorders', views.OrderNestedViewSet)
router.register(r'selecteditems', views.SelectedItemsViewSet)
# snippet_list = SnippetViewSet.as_view({
#     'get': 'list',
#     'post': 'create'
# })

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
  # url(r'^snippets/$', snippet_list, name='snippet-list'),
    url(r'^', include(router.urls)),
]