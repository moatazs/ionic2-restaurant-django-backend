from __future__ import unicode_literals

from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from stdimage.models import StdImageField
import os
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from restaurant.models import Restaurant,Table,Seat
from django.conf import settings

class Category(models.Model):
    name = models.CharField(max_length = 100)
    restaurant = models.ForeignKey(Restaurant, related_name='categories', on_delete=models.CASCADE)
    image = StdImageField(upload_to='categories/',blank=True, null=True,
                          variations={'thumbnail': {'width': 300, 'height': 150}})
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

class Item(models.Model):
    name = models.CharField(max_length = 100)
    description = models.TextField(blank=True)
    category = models.ForeignKey(Category, related_name='items', on_delete=models.CASCADE)
    image = StdImageField(upload_to='menuitems/',blank=True, null=True,
                          variations={'thumbnail': {'width': 300, 'height': 150}})
    price = models.DecimalField (max_digits=4, decimal_places=2)

    def __str__(self):
        return self.name

class Option(models.Model): 
    name = models.CharField(max_length = 100) # group name for radios, option name for checks
    required = models.BooleanField() # is radio?
    item = models.ForeignKey(Item, related_name='options', on_delete=models.CASCADE)

    class Meta:
        unique_together = ("name", "item")

    def __str__(self):
        return self.name

class ItemOption(models.Model):
    option = models.ForeignKey(Option, related_name='itemoptions', on_delete=models.CASCADE)
    value = models.CharField(max_length = 100) # option value
    price = models.DecimalField (max_digits=4, decimal_places=2) # if applied
    #the hint field can be used in case the item option itself has options. e.g.: baked potatoes
    #here we would put (please put in the comments section whether you want your baked potatoes
    # fully loaded or with which of the following: mayo, green opnions , ..etc)
    # the hint could be showns as an (i) icon on the side or with small font.
    hint = models.CharField(max_length = 200,blank=True, null=True)


    def __str__(self):
        return self.option.name + " " + self.value


class Order(models.Model):
    """
    An order list object that can be used to make separate orders that contain multiple items.
    each order can be open or closed and done.
    For now, the user will close the order him/her self
    later it could be a timer or linked to the POS system.
    """
    customer = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    seat = models.ForeignKey(Seat, on_delete=models.CASCADE)
    closed = models.BooleanField() # open or closed order
    created = models.DateTimeField(auto_now_add=True)
    paid = models.BooleanField() # open or closed order

    

class SelectedItem(models.Model):
    order = models.ForeignKey(Order, related_name='selecteditems', on_delete=models.CASCADE)
    item = models.ForeignKey(Item, related_name='selecteditems', on_delete=models.CASCADE)
    comments = models.TextField(blank=True) #extra comments from user "e.g. no bacon"
    created = models.DateTimeField(auto_now_add=True)
    itemoptions = models.ManyToManyField(ItemOption, related_name='selecteditems')
    # later will add updated field , so that the user will have a window to update his/her order

# class SelectedItemOption(models.Model):
#     order = models.ForeignKey(Order, on_delete=models.CASCADE)
#     item = models.ForeignKey(Item, on_delete=models.CASCADE)
#     option = models.ForeignKey(Option, on_delete=models.CASCADE)
#     value = models.CharField(max_length = 100) # option value
#     price = models.DecimalField (max_digits=4, decimal_places=2) # if applied



    # now we want to add itemoptions but for the user. ChosenItemOptions